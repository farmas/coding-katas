-----------------------------
Calling WebServices Kata
-----------------------------

The goal of this kata is to practice invoking a remote service. With each iteration try to use a different technique, programming language, platform, etc.

The program should ask for a zip code within the U.S. (for example 98103) and print the city, state and temperature. A WebService that you can use is: http://wsf.cdyne.com/WeatherWS/Weather.asmx