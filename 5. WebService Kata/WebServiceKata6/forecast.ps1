param([string] $zip)

$client = new-object System.Net.WebClient
$client.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
$resp = [xml] $client.UploadString("http://wsf.cdyne.com/WeatherWS/Weather.asmx/GetCityForecastByZIP", "POST", "ZIP=" + $zip)

write-host $resp.ForecastReturn.City "," $resp.ForecastReturn.State
$resp.ForecastReturn.ForecastResult.Forecast | foreach { 
   write-host (get-date $_.Date).DayOfWeek ":" $_.Temperatures.MorningLow "-" $_.Temperatures.DaytimeHigh "." $_.Desciption   
}
