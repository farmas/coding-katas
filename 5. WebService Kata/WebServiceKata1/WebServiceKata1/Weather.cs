﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebServiceKata1
{
    public class Weather
    {
        public string City { get; set; }
        public string State { get; set; }
        public int Temperature { get; set; }
    }
}
