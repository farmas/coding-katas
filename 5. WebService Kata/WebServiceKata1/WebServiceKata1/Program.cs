﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebServiceKata1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zip:");
            var zip = Console.ReadLine().Trim();

            var client = new RestSharp.RestClient("http://wsf.cdyne.com/WeatherWS/Weather.asmx");
            var request = new RestSharp.RestRequest("GetCityWeatherByZIP", RestSharp.Method.POST);
            request.AddParameter("ZIP", zip);

            Console.WriteLine("Sending request");
            var response = client.Execute<Weather>(request);

            Console.WriteLine(string.Format("City: {0}", response.Data.City));
            Console.WriteLine(string.Format("State: {0}", response.Data.State));
            Console.WriteLine(string.Format("Temperature: {0}", response.Data.Temperature));
        }
    }
}
