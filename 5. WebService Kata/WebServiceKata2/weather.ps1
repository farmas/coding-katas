param([string] $zip);

$dir = split-path $MyInvocation.MyCommand.Path

add-type -path (join-path $dir "RestSharp.dll")

$req = new-object RestSharp.RestRequest("GetCityWeatherByZIP", [RestSharp.Method]::POST)
$req.AddParameter("ZIP", $zip)

$client = new-object RestSharp.RestClient("http://wsf.cdyne.com/WeatherWS/Weather.asmx")
$resp = [xml]$client.Execute($req).Content

echo $resp.WeatherReturn.City
echo $resp.WeatherReturn.State
echo $resp.WeatherReturn.Temperature