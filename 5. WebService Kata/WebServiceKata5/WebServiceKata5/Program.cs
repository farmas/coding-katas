﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;

namespace WebServiceKata5
{
    public static class XLinqExtensions
    {
        private static XNamespace ns = "http://ws.cdyne.com/WeatherWS/";
        public static XElement ElementWithNS(this XElement parent, string name)
        {
            return parent.Element(ns + name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(("Zip:"));
            var zip = Console.ReadLine();

            var client = new WebClient();
            client.BaseAddress = "http://wsf.cdyne.com";
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            var resp = client.UploadString("/WeatherWS/Weather.asmx/GetCityForecastByZIP", "POST", "ZIP=" + zip);

            var root = XElement.Parse(resp);
            
            Console.WriteLine(root.ElementWithNS("City").Value + "," + root.ElementWithNS("State").Value);

            foreach (var e in root.ElementWithNS("ForecastResult").Elements())
            {
                var temperatures = e.ElementWithNS("Temperatures");
                Console.WriteLine(String.Format("{0}: {1}°-{2}°. {3}",
                    DateTime.Parse(e.ElementWithNS("Date").Value).DayOfWeek.ToString(),
                    temperatures.ElementWithNS("MorningLow").Value,
                    temperatures.ElementWithNS("DaytimeHigh").Value,
                    e.ElementWithNS("Desciption").Value
                    ));
            }

        }
    }
}
