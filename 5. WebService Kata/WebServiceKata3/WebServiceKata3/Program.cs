﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using System.IO;

namespace WebServiceKata3
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Zip:");
            var zip = Console.ReadLine();

            var client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            var resp = client.UploadString("http://wsf.cdyne.com/WeatherWS/Weather.asmx/GetCityWeatherByZIP", "POST", "ZIP=" + zip);
            
            var stream = new StringReader(resp);
            var elements = XElement.Load(new StringReader(resp)).Elements();

            Console.WriteLine(String.Format("City: {0}", elements.Where(e => e.Name.LocalName == "City").Single().Value));
            Console.WriteLine(String.Format("State: {0}", elements.Where(e => e.Name.LocalName == "State").Single().Value));
            Console.WriteLine(String.Format("Temperature: {0}", elements.Where(e => e.Name.LocalName == "Temperature").Single().Value));
        }
    }
}
