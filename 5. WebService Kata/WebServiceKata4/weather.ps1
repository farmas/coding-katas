param([string] $zip);

$client = new-object System.Net.WebClient
$client.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
$resp = [xml]$client.UploadString("http://wsf.cdyne.com/WeatherWS/Weather.asmx/GetCityWeatherByZIP", "POST", "ZIP=" + $zip)

write-host "City:" $resp.WeatherReturn.City
write-host "State:" $resp.WeatherReturn.State
write-host "Temperature:" $resp.WeatherReturn.Temperature