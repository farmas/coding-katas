$(document).ready(function(){
   var Issue = Backbone.Model.extend({
      defaults: {
         fixed: false
      }
   });
   
   var IssueView = Backbone.View.extend({
      events:{
         'click .completedCheckbox': 'toggleCompleted',
         'click .removeLink': 'remove'
      },
      initialize: function(){
         this.template = _.template($('#issue-template').html());
         this.model.bind('change', this.render, this);
      },
      render: function(){
         this.$el.html(this.template(this.model.toJSON()));
         return this;
      },
      toggleCompleted: function(){
         this.model.set({fixed: !this.model.get('fixed')});
      },
      remove: function(){
         this.model.destroy();
      }
   });
   
   var IssueCollection = Backbone.Collection.extend({
      model: Issue
   });
   
   var IssueCollectionView = Backbone.View.extend({
      tagName: 'ul',
      initialize: function(){
         this.collection.bind('add destroy', this.render, this);
      },
      render: function(){
         this.$el.empty();
         var that = this;
         this.collection.each(function(e){
            var issueView = new IssueView({ model: e });
            that.$el.append($('<li>').html(issueView.render().el));
         });
         return this;
      }
   });
   
   var addIssue = function(evt){
      var issue = new Issue({ 
         title: $('#newIssueText').val(),
         priority: $('#newIssuePriority').val()
      });
      
      issues.add(issue);
      
      $('#newIssueText').val('');
      evt.preventDefault();
   }
   
   var issues = new IssueCollection();
   var listView = new IssueCollectionView({collection: issues});
   
   $('.issueList').html(listView.render().el);

   $('#newIssueSubmit').click(addIssue);
   $('#newIssueForm').submit(addIssue);
   
   $('#newIssueText').focus();
 });