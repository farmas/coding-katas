------------------------------
Backbone Kata (work in progress)
------------------------------

1. User is able to create a new issue using the form. Created issues are displayed in a list

2. User is able to mark an issue as fixed. Issues that are fixed have their titles striked through.

3. User is able to delete an issue from the list.
