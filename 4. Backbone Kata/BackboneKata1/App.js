var Issue = Backbone.Model.extend({
	
});

var IssueView = Backbone.View.extend({
	tagName: 'li',
	initialize: function(){
		this.template = _.template($('#issue').html());
	},
	render: function(){
		$(this.el).html(this.template(this.model.toJSON()));
        
		return this;
	}
});

var IssueCollection = Backbone.Collection.extend({
	model: Issue
});

var IssueCollectionView = Backbone.View.extend({
	tagName: "ul",
	initialize: function(){
		this.collection.bind('add', this.render, this);
	},
	render: function(){		
		var $ul = $(this.el).empty();
		this.collection.each(function(i){
			$ul.append(new IssueView({model: i}).render().el);
		});
	
		return this;
	}
});

$(document).ready(function(){

	var issueCollection = new IssueCollection();
	var issueCollectionView = new IssueCollectionView({collection: issueCollection});

	$('#newIssueSubmit').click(function(evt){
		var issue = new Issue({
			title: $('#newIssueText').val(),
            priority: $('#newIssuePriority').find(':selected').val()
		});
		
		$('#newIssueText').val('');
		issueCollection.add(issue);
		
	});	

	$('.issueList').append(issueCollectionView.render().el);
});