------------------------------
Backbone Kata 
------------------------------

0. If you are new to backbone.js
	- Create a model for an "issue".
	- Create a view for the model using the template provided in the .html file
	- Display an issue on the screen with a title.
	- Use the console to update the title of the issue and have it reflect on the screen

1. User is able to create a new issue with title using the form. Created issues are displayed in a list

2. User is able to create a new issue with title and priority using the form. Created issues display their priority with icons.

3. User is able to mark an issue as fixed using the checkbox next to the issue. Issues that are fixed have their titles striked through.

4. User is able to delete an issue from the list.
