(function($, undefined){
   var setup = function($comment){
      if($comment.text().trim().length > 128){
         
         // store the full html of the comment
         $comment.find('p:last').append($('<a>').text('Collapse').addClass('expanded').attr('href', '#'));
         $comment.data('longhtml', $comment.html()); 
         
         var textLen = 0;
         $comment.find('p').each(function(i, p){
            var $p = $(p);
            if(textLen > 128){
               $p.remove();
               return;
            }
            var pLen = $p.text().trim().length;
            
            if(pLen + textLen > 128){
               $p.text($p.text().trim().substring(0, 128 - textLen));
               $p.append($('<a>').text('Expand').addClass('collapsed').attr('href', '#'));
            }
            textLen += pLen;
         });
         
         //store the short html of the comment
         $comment.data('shorthtml', $comment.html());
      }
   };

   $(document).ready(function(){
      $('.comment').each(function(index, el){
         setup($(el));
      });
      
      $('.comment').on('click','.expanded', function(evt){
         var $comment = $(evt.target).parents('.comment');
         $comment.fadeOut(function(){
            $comment.html($comment.data('shorthtml'));
            $comment.fadeIn();
         });
      });
      
      $('.comment').on('click','.collapsed', function(evt){
         var $comment = $(evt.target).parents('.comment');
         $comment.fadeOut(function(){
            $comment.html($comment.data('longhtml'));
            $comment.fadeIn();
         });
      });
   });
})(jQuery);