
$(document).ready(function(){
    var collapseComment = function($comment){
        var len = 0;
        
        $comment.find('>p').each(function(i, p){
            var $p = $(p);
            
            if(len > 128){
                // if already above char limit, hide all subsequent p's
                $p.hide(); 
                return;
            }
            
            var pText = $p.text().trim();
            if(len + pText.length > 128){
                $p.data('originalText', pText); // store the original text
                $p.text(pText.substring(0, 128 - len));  // set text to truncated string
                $('<a class="expand" href="#">  Expand...</a>').appendTo($p); // add the expand link
            }
            len += pText.length;
        });
    };
    
    $('.comments').on('click', '.expand', function(e){
        var $p = $(e.target).parent('p');
        
        $p.text($p.data('originalText')); // restore text
        
        var $lastP = $p.parents('.comment').children()
            .show() // show all siblings
            .last(); // get the last
            
        $('<a class="collapse" href="#">  Collapse...</a>').appendTo($lastP);
    });
    
    $('.comments').on('click', '.collapse', function(e){
        var $anchor = $(e.target);
        
        collapseComment($anchor.parents('.comment'));
        
        $anchor.remove();
    });
    
    // by default, collapse all comments
     $('.comment').each(function(i,comment){
        collapseComment($(comment));
    });
});