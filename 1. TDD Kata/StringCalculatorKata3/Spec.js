describe('Add', function () {

    function createStringCalculator() {
        return new StringCalculator();
    }

    describe('with default delimiter', function () {
        it('empty string should return 0', function () {
            var cal = createStringCalculator();

            var result = cal.add("");

            expect(result).toEqual(0);
        });

        it('single number should return the same number', function () {
            var cal = createStringCalculator();

            var result = cal.add("1");
            expect(result).toEqual(1);

            result = cal.add("2");
            expect(result).toEqual(2);
        });

        it('multiple numbers should return the sum', function () {
            var cal = createStringCalculator();

            var result = cal.add("1,2");
            expect(result).toEqual(3);

            result = cal.add("1,2,3,4");
            expect(result).toEqual(10);
        });

        it('multiple numbers with new line separator should return the sum', function () {
            var cal = createStringCalculator();

            var result = cal.add('1\n2');

            expect(result).toEqual(3);
        });

        it('multiple numbers with mix of new line and comma separators should return the sum', function () {
            var cal = createStringCalculator();

            var result = cal.add('1\n2,3');
            expect(result).toEqual(6);

            result = cal.add('1,2\n3');
            expect(result).toEqual(6);
        });

        it('single negative number should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('-1'); }).toThrow(new Error('negatives not allowed: -1'));

            expect(function () { cal.add('-2'); }).toThrow(new Error('negatives not allowed: -2'));
        });

        it('multiple negative numbers should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('-1,-2'); }).toThrow(new Error('negatives not allowed: -1,-2'));

            expect(function () { cal.add('-1,2,-3'); }).toThrow(new Error('negatives not allowed: -1,-3'));
        });

        it('multiple negative numbers with mix delimeters should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('-1,2\n-3'); }).toThrow(new Error('negatives not allowed: -1,-3'));
        });
    });

    describe('with custom delimiter', function () {
        it('empty string should return 0', function () {
            var cal = createStringCalculator();

            var result = cal.add('//;\n');

            expect(result).toEqual(0);
        });

        it('single number should return the same number', function () {
            var cal = createStringCalculator();

            var result = cal.add('//;\n1');

            expect(result).toEqual(1);
        });

        it('multiple numbers should return the sum', function () {
            var cal = createStringCalculator();

            var result = cal.add('//;\n1;2');

            expect(result).toEqual(3);
        });

        it('multiple numbers with mix of custom delimeter and new line should return the sum', function () {
            var cal = createStringCalculator();

            var result = cal.add('//;\n1;2\n3;4');

            expect(result).toEqual(10);
        });

        it('single negative number should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('//;\n-1'); }).toThrow(new Error('negatives not allowed: -1'));
        });

        it('multiple negative numbers should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('//;\n-1;-2'); }).toThrow(new Error('negatives not allowed: -1,-2'));

            expect(function () { cal.add('//;\n-1;2;-3'); }).toThrow(new Error('negatives not allowed: -1,-3'));
        });

        it('multiple negative numbers with mix delimeters should throw exception', function () {
            var cal = createStringCalculator();

            expect(function () { cal.add('//;\n-1;2\n-3'); }).toThrow(new Error('negatives not allowed: -1,-3'));
        });
    });
});
