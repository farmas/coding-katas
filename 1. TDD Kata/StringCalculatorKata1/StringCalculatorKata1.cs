﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace StringCalculatorKata1
{
    public class StringCalculatorKataTest
    {
        private static StringCalculator CreateCalculator()
        {
            return new StringCalculator();
        }

        [Fact]
        public void Add_EmptyString_ReturnsZero()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("");

            Assert.Equal(0, result);
        }

        [Fact]
        public void Add_SingleNumber_ReturnsNumber()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("1");

            Assert.Equal(1, result);
        }

        [Fact]
        public void Add_TwoNumbers_ReturnsSum()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("1,2");

            Assert.Equal(3, result);
        }

        [Fact]
        public void Add_AnyCountOfNumbers_ReturnsSum()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("1,2,3,4,5");

            Assert.Equal(15, result);
        }

        [Fact]
        public void Add_NumbersSeparatedByNewLine_ReturnsSum()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("1\n2");

            Assert.Equal(3, result);
        }

        [Fact]
        public void Add_NumbersSeparatedByMixOfNewLineAndCommas_ReturnsSum()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("1\n2,3");

            Assert.Equal(6, result);
        }

        [Fact]
        public void Add_CustomDelimiterWithNoNumber_ReturnsZero()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("//;\n");

            Assert.Equal(0, result);
        }

        [Fact]
        public void Add_CustomDelimiterWithSingleNumber_ReturnsNumber()
        {
            var calculator = CreateCalculator();

            int result = calculator.Add("//;\n1");

            Assert.Equal(1, result);
        }

        [Fact]
        public void Add_CustomDelimiterWithNegativeNumber_ThrowsException()
        {
            var calculator = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calculator.Add("//;\n-1"));

            Assert.Equal("negatives not allowed: -1", exception.Message);
        }

        [Fact]
        public void Add_NegativeNumber_ThrowsException()
        {
            var calculator = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calculator.Add("-1"));

            Assert.Equal("negatives not allowed: -1", exception.Message);
        }

        [Fact]
        public void Add_AllNegativeNumbers_ThrowsExceptionWithListOfNumbers()
        {
            var calculator = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calculator.Add("-1,-2,-3"));

            Assert.Equal("negatives not allowed: -1,-2,-3", exception.Message);
        }

        [Fact]
        public void Add_NegativeAndPositiveNumbers_ThrowsExceptionWithListOfNegativeNumbers()
        {
            var calculator = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calculator.Add("1,2,-3,4,5,-6"));

            Assert.Equal("negatives not allowed: -3,-6", exception.Message);
        }
    }

    public class StringCalculator
    {
        internal int Add(string numbers)
        {
            string delimiter = ",";
            if (numbers.StartsWith("//"))
            {
                delimiter = numbers.Substring(2, 1);
                numbers = numbers.Substring(4);
            }
            
            return AddNumbersBetweenDelimiter(numbers, delimiter);
        }

        internal int AddNumbersBetweenDelimiter(string numbers, string delimiter)
        {
            if (String.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            int sum = 0;
            List<int> errors = new List<int>();

            foreach(int n in numbers.Split(new string[] { delimiter, "\n"}, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)))
            {   
                if(n >= 0)
                {
                    sum += n;                                        
                }
                else
                {
                    errors.Add(n);
                }
            }

            if(errors.Count > 0)
            {
                throw new InvalidOperationException(String.Format(
                    "negatives not allowed: {0}", 
                    String.Join(",", errors.ToArray())));
            }
            else
            {
                return sum;
            }
        }
    }
}
