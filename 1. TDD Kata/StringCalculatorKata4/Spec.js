describe('Add', function () {
	var calc = new StringCalculator();
	
	describe('default delimiters', function(){
		it('empty string returns 0', function(){		
			expect(calc.add("")).toEqual(0);
		});
		
		it('single number returns same number', function(){		
			expect(calc.add("1")).toEqual(1);
			expect(calc.add("2")).toEqual(2);
		});
		
		it('multiple numbers with comma delimiter returns the sum', function(){		
			expect(calc.add("1,2")).toEqual(3);
			expect(calc.add("1,2,3")).toEqual(6);
		});
		
		it('multiple numbers with new line delimiter returns the sum', function(){	
			expect(calc.add("1\n2")).toEqual(3);
			expect(calc.add("1\n2\n3")).toEqual(6);
		});
		
		it('multiple numbers with mix of delimiters returns the sum', function(){	
			expect(calc.add("1\n2,3")).toEqual(6);
		});
		
		it('single negative number should throw error', function(){	
			expect(function() { calc.add("-1")}).toThrow(new Error('negatives not allowed: -1'));
		});
		
		it('multiple negative numbers should throw error', function(){	
			expect(function() { calc.add("-1,-2")}).toThrow(new Error('negatives not allowed: -1,-2'));
		});
		
		it('Mix of negative numbers and positive numbers should throw error', function(){	
			expect(function() { calc.add("1,-2\n3,-4")}).toThrow(new Error('negatives not allowed: -2,-4'));
		});
	});
	
	describe('custom delimiters', function(){
		it('empty string returns 0', function(){		
			expect(calc.add("//;\n")).toEqual(0);
		});
		
		it('single number returns the same number', function(){		
			expect(calc.add("//;\n1")).toEqual(1);
		});
		
		it('multiple numbers returns the sum', function(){		
			expect(calc.add("//;\n1;2")).toEqual(3);
			expect(calc.add("//;\n1;2;3")).toEqual(6);
		});
		
		it('multiple numbers with mix delimiters returns the sum', function(){		
			expect(calc.add("//;\n1;2\n3")).toEqual(6);
		});
		
		it('single negative number should throw error', function(){	
			expect(function() { calc.add("//;\n-1")}).toThrow(new Error('negatives not allowed: -1'));
		});
		
		it('multiple negative numbers should throw error', function(){	
			expect(function() { calc.add("//;\n-1;-2")}).toThrow(new Error('negatives not allowed: -1,-2'));
		});
		
		it('Mix of negative numbers and positive numbers should throw error', function(){	
			expect(function() { calc.add("//;\n1;-2\n3;-4")}).toThrow(new Error('negatives not allowed: -2,-4'));
		});
	});
});