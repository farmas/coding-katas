function StringCalculator(){
	this.add = function(numbers){
		var delimiter = ',';
		
		if(numbers.indexOf('//') == 0){
			delimiter = numbers.substr(2,1);
			numbers = numbers.substr(4);
		}
	
		if(numbers.length == 0){
			return 0;
		}
		
		var sum = 0;
		var negatives = [];
		_.chain(numbers.split(delimiter))
			.map(function(n){ return n.split('\n'); })
			.flatten()
			.each(function(n){
				var num = parseInt(n);
				
				if(num < 0){
					negatives.push(num);
				}
				else{
					sum += num;
				}
			});
				
		if(negatives.length > 0){
			throw "negatives not allowed: " + negatives.join(',');
		}
		
		return sum;							
	}
}