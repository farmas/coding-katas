﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Extensions;

namespace StringCalculatorKata2
{
    public class StringCalculatorKata2
    {
        private static StringCalculator CreateCalculator()
        {
            return new StringCalculator();
        }

        [Fact]
        public void Add_EmtpyString_ReturnsZero()
        {
            var calc = CreateCalculator();

            int result = calc.Add("");

            Assert.Equal(0, result);
        }

        [Theory]
        [InlineData(new object[] { "1", 1 })]
        [InlineData(new object[] { "2", 2 })]
        public void Add_SingleNumber_ReturnsNumber(string input, int expected)
        {
            var calc = CreateCalculator();

            int result = calc.Add(input);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(new object[] { "1,2", 3 })]
        [InlineData(new object[] { "2,3", 5 })]
        [InlineData(new object[] { "1,2,3,4", 10 })]
        public void Add_MultipleNumbers_ReturnsSum(string input, int expected)
        {
            var calc = CreateCalculator();

            int result = calc.Add(input);

            Assert.Equal(result, expected);
        }

        [Theory]
        [InlineData(new object[] { "1\n2", 3 })]
        [InlineData(new object[] { "1\n2,3", 6 })]
        [InlineData(new object[] { "1,2\n3", 6 })]
        public void Add_MultipleNumbersWithCommaAndNewLinesAsSeparators_ReturnsSum(string input, int expected)
        {
            var calc = CreateCalculator();

            int result = calc.Add(input);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void Add_EmptyStringWithCustomDelimiter_ReturnZero()
        {
            var calc = CreateCalculator();

            int result = calc.Add("//;\n");

            Assert.Equal(0, result);
        }

        [Fact]
        public void Add_SingleNumberWithCustomDelimiter_ReturnsNumber()
        {
            var calc = CreateCalculator();

            int result = calc.Add("//;\n1");

            Assert.Equal(1, result);
        }

        [Theory]
        [InlineData(new object[] { "//;\n1;2", 3 })]
        [InlineData(new object[] { "//;\n1;2;3;4", 10 })]
        [InlineData(new object[] { "//;\n1\n2;3\n4", 10 })]
        public void Add_MultipleNumbersWithCustomDelimiter_ReturnsNumber(string input, int expected)
        {
            var calc = CreateCalculator();

            int result = calc.Add(input);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(new object[] { "-1" })]
        [InlineData(new object[] { "//;\n-1" })]
        public void Add_SingleNegativeNumber_ThrowsException(string input)
        {
            var calc = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calc.Add(input));

            Assert.Equal("negatives not allowed: -1", exception.Message);
        }

        [Theory]
        [InlineData(new object[] { "-1,-2", "-1,-2" })]
        [InlineData(new object[] { "//;\n-1;-2", "-1,-2" })]
        [InlineData(new object[] { "-1,2,-3,4", "-1,-3" })]
        [InlineData(new object[] { "//;\n1;-2;3;-4", "-2,-4" })]
        public void Add_MultipleNumbersWithNegatives_ThrowsExceptionWithListOfNegativeNumbers(string input, string expected)
        {
            var calc = CreateCalculator();

            var exception = Assert.Throws<InvalidOperationException>(() => calc.Add(input));

            Assert.Equal(String.Format("negatives not allowed: {0}", expected), exception.Message);
        }
    }

    public class StringCalculator
    {
        internal int Add(string numbers)
        {
            string delimiter = ExtractDelimiter(ref numbers);

            if (String.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            return Add(numbers, delimiter);

        }

        private static int Add(string numbers, string delimiter)
        {
            int sum = 0;
            List<int> negativeNumbers = new List<int>();
            foreach (int n in numbers.Split('\n', delimiter[0]).Select(s => int.Parse(s)))
            {
                if (n < 0)
                {
                    negativeNumbers.Add(n);
                }
                else
                {
                    sum += n;
                }
            }

            if (negativeNumbers.Count == 0)
            {
                return sum;
            }
            else
            {
                throw new InvalidOperationException(
                    String.Format(
                        "negatives not allowed: {0}",
                        String.Join(",", negativeNumbers.ToArray())));
            }
        }

        private static string ExtractDelimiter(ref string numbers)
        {
            string delimiter = ",";
            if (numbers.StartsWith("//"))
            {
                delimiter = numbers.Substring(2, 1);
                numbers = numbers.Substring(4);
            }
            return delimiter;
        }
    }
}
