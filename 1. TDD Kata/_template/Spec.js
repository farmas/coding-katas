/*
Underscore: http://documentcloud.github.com/underscore/
Jasmine: https://github.com/pivotal/jasmine/wiki/Matchers
*/

describe('Sample', function(){
	var foo;
	
	beforeEach(function () {
		foo = 0;
	});

	it('should increment a variable', function(){
		foo++;
		expect(foo).toEqual(1);
	});
});