﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class TodoListApiController : ApiController
    {
        private static IDictionary<int, TodoItem> _todoList = new Dictionary<int, TodoItem>() 
        {
            { 0, new TodoItem() { Id = 0, Title = "First Item" }},
            { 1, new TodoItem() { Id = 1, Title = "Second Item" }}
        };

        public ICollection<TodoItem> GetItems()
        {
            return _todoList.Values;
        }

        public TodoItem PostItem(TodoItem todoItem)
        {
            todoItem.Id = _todoList.Values.Max(i => i.Id) + 1;
            _todoList.Add(todoItem.Id, todoItem);
            return todoItem;
        }

        public void PutItem(TodoItem todoItem)
        {
            if (_todoList.ContainsKey(todoItem.Id))
            {
                _todoList[todoItem.Id] = todoItem;
            }
        }

        public void DeleteItem(int id)
        {
            if (_todoList.ContainsKey(id))
            {
                _todoList.Remove(id);
            }
        }
    }
}
