﻿(function($, ko) {
    var template = "<form class='todoList-controls' data-bind='submit: addItem'>" +
            "Task: <input type='textbox' class='todoList-newTaskTitle' />" +
            "<input type='submit' value='Add' />" +
        "</form>" +
        "<ul class='todoList-tasks' data-bind='foreach: tasks'>" +
            "<li class='todoList-task'>" +
                "<span data-bind='text: title, click: toggleCompleteItem, css: { complete: complete() }'></span>" +
                "<a href='#' data-bind='click: $root.removeItem'>X</a>" +
            "</li>" +
        "</ul>";

    var Task = function(title, id, complete) {
        this.id = ko.observable(id);
        this.title = ko.observable(title);
        this.complete = ko.observable(complete || false);

        this.toggleCompleteItem = function(item) {
            item.complete(!item.complete());
        }

        var updateSubscription = ko.computed(function() {
            var changes = this.title() + this.complete();

            if (updateSubscription) {
                $.ajax({
                    url: '/api',
                    data: ko.toJSON(this),
                    contentType: 'application/json',
                    type: 'PUT'
                });
            }
            
        }, this);
    };

    var TodoList = function() {
        var self = this;
        this.tasks = ko.observableArray([]);

        this.addItem = function(element) {
            var $taskTextbox = $(element).find(".todoList-newTaskTitle"),
                title = $taskTextbox.val(),
                task = new Task(title);

            self.tasks.push(task);
            $taskTextbox.val('');
            
            $.ajax({
                url: '/api',
                data: ko.toJSON(task),
                contentType: 'application/json',
                type: 'POST'
            }).done(function(newTask) {
                task.id(newTask.id);
            }).fail(function(error) {
                console.log(error);
            });
        };

        this.removeItem = function(item) {
            self.tasks.remove(item);

            $.ajax({
                url: '/api?id=' + item.id(),
                type: 'DELETE'
            });
        }
    }

    $.fn.todoList = function() {
        return this.each(function() {
            var todoList = new TodoList();

            $(this).html(template);
            ko.applyBindings(todoList, this);

            $.ajax({
                url: '/api'
            }).done(function(tasks) {
                $.map(tasks, function(task) {
                    todoList.tasks.push(new Task(task.title, task.id, task.complete));
                });
            });
        });
    };
})(jQuery, ko);